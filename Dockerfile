FROM almalinux/9-base

RUN dnf update -y
RUN dnf install python python-pip -y
RUN dnf clean all

COPY . /home/pedro/biorecords

WORKDIR /home/pedro/biorecords

RUN pip install -r requirements_cpu.txt

CMD python server.py
