from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd
import torch
import os
import numpy as np
import matplotlib.pyplot as plt
import hydra
import tqdm

import training
import torchvision


@hydra.main(config_path="./cfg", config_name="config")
def main(cfg):
    device = "cuda:0"
    _, model_path = cfg.server.endpoints_models.items()[0]
    net = training.Model.load_from_checkpoint(model_path).to(device)

    y_pred = []
    y_true = []

    valid_dataset = torchvision.datasets.ImageFolder(
        os.path.join(cfg.dataset.root, cfg.dataset.valid),
        transform=training.build_resize(
            cfg.model.resolution,
            cfg.dataset.normalization.mean,
            cfg.dataset.normalization.std,
        ),
    )

    valid_loader = torch.utils.data.DataLoader(
        valid_dataset,
        batch_size=32,
        num_workers=cfg.system.num_workers,
        pin_memory=True if torch.cuda.is_available() else False,
    )

    # iterate over test data
    for inputs, labels in tqdm.tqdm(valid_loader):
        inputs, labels = inputs.to(device), labels.to(device)
        output = net(inputs)  # Feed Network

        output = (torch.max(torch.exp(output), 1)[1]).data.cpu().numpy()
        y_pred.extend(output)  # Save Prediction

        labels = labels.data.cpu().numpy()
        y_true.extend(labels)  # Save Truth
    
    occurence= []
    for lbl in set(y_true):
        count=y_true.count(lbl)
        occurence.append((lbl, count))
    occurence = sorted(occurence, key=lambda x: x[1])
    print(occurence, sep='\n')

    # constant for classes
    classes = sorted(net.idx_to_class.items(), key=lambda x: x[0])

    # Build confusion matrix
    cf_matrix = confusion_matrix(y_true, y_pred)
    df_cm = pd.DataFrame(
        cf_matrix / np.sum(cf_matrix) * 10,
        index=[i for i in classes],
        columns=[i for i in classes],
    )
    plt.figure(figsize=(30, 30))
    # sn.heatmap(df_cm, annot=True)
    sn.heatmap(df_cm, annot=False)
    plt.savefig("/home/pedro/biorecords/workdir/confussion.png")


if __name__ == "__main__":
    main()
