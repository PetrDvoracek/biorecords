import flask
import io
from PIL import Image
import numpy as np
import hydra
import torch
import sys

import training

app = flask.Flask(__name__)


def register_predict_endpoint(model, endpoint):
    model.eval()  # inplace

    @app.route(f"/{endpoint}", endpoint=endpoint, methods=["POST"])
    def predict_endpoint():
        img_req = flask.request.files["image"]
        with io.BytesIO() as f:
            img_req.save(f)
            img_pil = Image.open(f).convert(
                "RGB"
            )  # Deal with RGBA, A is 4th channel for transparency
            img_np = np.asarray(img_pil, dtype="uint8")

        preproc = training.build_resize(
            resolution=model.cfg.model.resolution,
            mean=model.cfg.dataset.normalization.mean,
            std=model.cfg.dataset.normalization.std,
        )

        batch = preproc(img_np).unsqueeze(0)
        with torch.no_grad():
            prediction = model(batch)
        predicted_class_idx = torch.argmax(prediction, dim=1)
        assert len(predicted_class_idx) == 1  # only one image in one request

        predicted_class_idx = predicted_class_idx.squeeze_(0).item()
        predicted_class_name = model.idx_to_class[predicted_class_idx]

        return flask.jsonify(
            {
                "predicted_class": predicted_class_name,
                "predicted_idx": predicted_class_idx,
            }
        )

    return predict_endpoint


@hydra.main(config_path="./cfg", config_name="config")
def main(cfg):
    app.config.from_mapping(cfg)
    app.config["config"] = cfg
    for endpoint, model_path in cfg.server.endpoints_models.items():
        _ = register_predict_endpoint(
            model=training.Model.load_from_checkpoint(model_path),
            endpoint=endpoint,
        )
    app.run(host=cfg.server.host, port=cfg.server.port, debug=True)


if __name__ == "__main__":
    sys.argv.append(
        "hydra.run.dir=."
    )  # solve server.py not found flask error (current workdir in project root)
    main()
