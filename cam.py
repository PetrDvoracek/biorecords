import torch
import torchvision
import training
import cv2
import numpy as np
import matplotlib.pyplot as plt
import hydra

import os
import glob

MODEL_PATH = "/home/pedro/biorecords/workdir/origres_dragonfly_tf_efficientnet_b5-date-2022-12-02-time-22-40-20/origres_dragonfly_tf_efficientnet_b5_epoch=19_val_accuracy.top1=0.866.ckpt"
# IM_PATHS_WILD = f"/home/pedro/biorecords/fotky_rucne/vazky/*/*"
IM_PATHS_WILD = f"/home/pedro/biorecords/fotky_rucne/vazky/*/*"


def grad_cam(
    model,
    module,
    orig_image,
    transform,
    device="cuda",
):

    acts = [0]
    grads = [0]

    def f_hook(self, input, output):
        acts[0] = output

    def b_hook(self, grad_in, grad_out):
        grads[0] = grad_out

    h1 = module.register_forward_hook(f_hook)
    h2 = module.register_backward_hook(b_hook)

    img_t = transform(orig_image).to(device)
    model.to(device)
    outs = model(img_t.unsqueeze(0))
    h1.remove()
    h2.remove()
    pred_idx = outs[0].argmax()
    outs[0, pred_idx].backward()

    gap = torch.mean(
        grads[0][0].view(grads[0][0].size(0), grads[0][0].size(1), -1), dim=2
    )
    acts = acts[0][0]
    gradcam = torch.nn.ReLU()(
        torch.sum(gap[0].reshape((gap.size()[1], 1, 1)) * acts, dim=0)
    )
    arr = torchvision.transforms.Resize((orig_image.shape[0], orig_image.shape[1]))(
        gradcam.unsqueeze(0)
    )
    gradcam_img = arr.detach().cpu().permute((1, 2, 0)).squeeze(-1)

    return pred_idx, gradcam_img


def main():

    for im_path in glob.glob(IM_PATHS_WILD):
        if "prediction_" in im_path:
            continue

        *workdir, im_filename = im_path.split(os.sep)
        im_name = im_filename.split(".")[0]
        workdir = os.sep.join(workdir)

        model = training.Model.load_from_checkpoint(MODEL_PATH)
        model.eval()
        preproc = training.build_resize(
            resolution=model.cfg.model.resolution,
            mean=model.cfg.dataset.normalization.mean,
            std=model.cfg.dataset.normalization.std,
        )

        image = cv2.imread(im_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        pred, cam = grad_cam(
            model.model,
            model.model.act2,
            image,
            transform=preproc,
        )
        pred_str = model.idx_to_class[pred.item()]

        figsize = (16, 16)
        alpha = 0.4
        fig, axs = plt.subplots(1, figsize=figsize)
        axs.imshow(np.asarray(image))
        axs.imshow(cam, alpha=alpha)
        axs.set_xticks([])
        axs.set_yticks([])
        axs.set_title(pred_str.replace("_", " ").title(), fontdict={"fontsize": 40})
        fig.savefig(os.path.join(workdir, f"prediction_{im_name}.png"))


if __name__ == "__main__":
    main()
