from dataclasses import dataclass


@dataclass
class Wandb:
    project: str
    entity: str
    mode: str


@dataclass
class Exp:
    name: str
    timm_model_name: str
    resolution: int
    batch_size: int
    train_max_epochs: int
    learning_rate: float


@dataclass
class System:
    cuda_device: int
    num_workers: int


@dataclass
class Normalization:
    mean: list
    std: list


@dataclass
class Dataset:
    root: str
    train: str
    test: str
    normalization: Normalization


@dataclass
class Augmentation:
    normalization: Normalization


@dataclass
class Config:
    wandb: Wandb
    exp: Exp
    system: System
    dataset: Dataset
    augmentation: Augmentation
