import timm
import pytorch_lightning as pl
import torch
import torchvision
import albumentations as A
from albumentations.pytorch import ToTensorV2
import numpy as np
import hydra
import omegaconf
import cv2

import os

from PIL import ImageFile

# fix error OSError: image file is truncated (0 bytes not processed)
ImageFile.LOAD_TRUNCATED_IMAGES = True


class FocalLoss(torch.nn.Module):
    def __init__(self, weight=None, gamma=2.0, reduction="none"):
        torch.nn.Module.__init__(self)
        self.weight = weight
        self.gamma = gamma
        self.reduction = reduction

    def __call__(self, input_tensor, target_tensor):
        log_prob = torch.nn.functional.log_softmax(input_tensor, dim=-1)
        prob = torch.exp(log_prob)
        return torch.nn.functional.nll_loss(
            ((1 - prob) ** self.gamma) * log_prob,
            target_tensor,
            weight=self.weight,
            reduction=self.reduction,
        )


criterion = torch.nn.CrossEntropyLoss()


def build_resize(resolution, mean, std):
    albu_f = A.Compose(
        [
            A.LongestMaxSize(max_size=resolution, interpolation=cv2.INTER_CUBIC),
            A.PadIfNeeded(
                min_height=resolution,
                min_width=resolution,
                border_mode=0,
                value=(0, 0, 0),
            ),
            A.Normalize(
                mean=mean,
                std=std,
            ),
            ToTensorV2(),
        ]
    )
    return albu2transform(albu_f)


def build_augment(resolution, mean, std):
    n_interpolations = 5
    albu_f = A.Compose(
        [
            A.LongestMaxSize(max_size=resolution, interpolation=cv2.INTER_CUBIC),
            A.PadIfNeeded(
                min_height=resolution,
                min_width=resolution,
                border_mode=0,
                value=(0, 0, 0),
            ),
            A.OneOf(
                [
                    A.RandomResizedCrop(
                        resolution,
                        resolution,
                        scale=(0.4, 1.4),
                        p=1 / n_interpolations,
                        interpolation=x,
                    )
                    for x in range(0, n_interpolations)
                ],
                p=0.5,
            ),
            # A.OneOf(
            #     [A.GaussNoise(p=0.3), A.MultiplicativeNoise(p=0.3), A.ISONoise(p=0.3)],
            #     p=1.0,
            # ),
            # A.RandomGamma(p=0.2),
            # A.RandomToneCurve(p=0.2),
            A.Cutout(num_holes=16, max_h_size=24, max_w_size=42, p=0.7),
            A.OneOf(
                [
                    A.Rotate(limit=20, interpolation=x, p=1 / n_interpolations)
                    for x in range(0, n_interpolations)
                ],
                p=0.5,
            ),
            A.OneOf(
                [
                    A.AdvancedBlur(p=1 / 2),
                    A.GaussianBlur(p=1 / 2),
                ],
                p=0.5,
            ),
            A.Sharpen(p=0.5),
            A.HorizontalFlip(),
            A.ImageCompression(quality_lower=70, p=0.5),
            A.Normalize(mean=mean, std=std),
            ToTensorV2(),
        ]
    )
    return albu2transform(albu_f)


def albu2transform(f):
    def t(image):
        image = np.array(image)
        return f(image=image)["image"]

    return t


def accuracy(output: torch.Tensor, target: torch.Tensor, topk=(1,)):
    """
    Computes the accuracy over the k top predictions for the specified values of k
    In top-5 accuracy you give yourself credit for having the right answer
    if the right answer appears in your top five guesses.

    ref:
    - https://pytorch.org/docs/stable/generated/torch.topk.html
    - https://discuss.pytorch.org/t/imagenet-example-accuracy-calculation/7840
    - https://gist.github.com/weiaicunzai/2a5ae6eac6712c70bde0630f3e76b77b
    - https://discuss.pytorch.org/t/top-k-error-calculation/48815/2
    - https://stackoverflow.com/questions/59474987/how-to-get-top-k-accuracy-in-semantic-segmentation-using-pytorch

    :param output: output is the prediction of the model e.g. scores, logits, raw y_pred before normalization or getting classes
    :param target: target is the truth
    :param topk: tuple of topk's to compute e.g. (1, 2, 5) computes top 1, top 2 and top 5.
    e.g. in top 2 it means you get a +1 if your models's top 2 predictions are in the right label.
    So if your model predicts cat, dog (0, 1) and the true label was bird (3) you get zero
    but if it were either cat or dog you'd accumulate +1 for that example.
    :return: list of topk accuracy [top1st, top2nd, ...] depending on your topk input
    """
    with torch.no_grad():
        # ---- get the topk most likely labels according to your model
        # get the largest k \in [n_classes] (i.e. the number of most likely probabilities we will use)
        maxk = max(
            topk
        )  # max number labels we will consider in the right choices for out model
        batch_size = target.size(0)

        # get top maxk indicies that correspond to the most likely probability scores
        # (note _ means we don't care about the actual top maxk scores just their corresponding indicies/labels)
        _, y_pred = output.topk(k=maxk, dim=1)  # _, [B, n_classes] -> [B, maxk]
        y_pred = (
            y_pred.t()
        )  # [B, maxk] -> [maxk, B] Expects input to be <= 2-D tensor and transposes dimensions 0 and 1.

        # - get the credit for each example if the models predictions is in maxk values (main crux of code)
        # for any example, the model will get credit if it's prediction matches the ground truth
        # for each example we compare if the model's best prediction matches the truth. If yes we get an entry of 1.
        # if the k'th top answer of the model matches the truth we get 1.
        # Note: this for any example in batch we can only ever get 1 match (so we never overestimate accuracy <1)

        target_reshaped = target.view(1, -1).expand_as(
            y_pred
        )  # [B] -> [B, 1] -> [maxk, B]
        # compare every topk's model prediction with the ground truth & give credit if any matches the ground truth
        correct = (
            y_pred == target_reshaped
        )  # [maxk, B] were for each example we know which topk prediction matched truth
        # original: correct = pred.eq(target.view(1, -1).expand_as(pred))

        # -- get topk accuracy
        list_topk_accs = []  # idx is topk1, topk2, ... etc
        for k in topk:
            # get tensor of which topk answer was right
            ind_which_topk_matched_truth = correct[:k]  # [maxk, B] -> [k, B]
            # flatten it to help compute if we got it correct for each example in batch
            flattened_indicator_which_topk_matched_truth = (
                ind_which_topk_matched_truth.reshape(-1).float()
            )  # [k, B] -> [kB]
            # get if we got it right for any of our top k prediction for each example in batch
            tot_correct_topk = flattened_indicator_which_topk_matched_truth.float().sum(
                dim=0, keepdim=True
            )  # [kB] -> [1]
            # compute topk accuracy - the accuracy of the mode's ability to get it right within it's top k guesses/preds
            topk_acc = tot_correct_topk / batch_size  # topk accuracy for entire batch
            list_topk_accs.append(topk_acc)
    return list_topk_accs  # list of topk accuracies for entire batch [topk1, topk2, ... etc]


class Model(pl.LightningModule):
    def __init__(
        self, num_classes, cfg, idx_to_class, cutmixup=False, pretrained=False
    ):
        super().__init__()
        self.save_hyperparameters()
        self.cfg = cfg
        self.idx_to_class = idx_to_class
        self.model = timm.create_model(
            cfg.model.timm_name,
            pretrained=pretrained,
            in_chans=3,
            num_classes=num_classes,
        )
        if cutmixup:
            self.cutmixup = timm.data.mixup.Mixup(
                mixup_alpha=0.3,  # TODO use 0.3
                cutmix_alpha=0.3,
                # cutmix_minmax=(0.4, 1.4),
                prob=0.9,
                switch_prob=0.5,
                mode="batch",
                label_smoothing=0.01,
                num_classes=num_classes,
            )
        else:
            self.cutmixup = None

    def forward(self, x):
        x = self.model(x)
        return x

    def configure_optimizers(self):
        optimizer = eval(self.cfg.optimizer.class_name)(
            self.parameters(), **self.cfg.optimizer.kwargs
        )
        step_fractions = [0.8, 0.9, 0.95]
        lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(
            optimizer,
            milestones=[int(self.cfg.exp.train_max_epochs * x) for x in step_fractions],
            gamma=0.2,
        )
        return [optimizer], [lr_scheduler]

    def training_step(self, batch, idx):
        inputs, labels = batch
        if self.cutmixup is not None:
            inputs, labels = self.cutmixup(inputs, labels)
        outputs = self.forward(inputs)
        loss = criterion(outputs, labels)

        # cannot measure accuracy because of cutmixup

        self.log("train_loss", loss.item(), on_epoch=True, on_step=False)
        if idx == 1 and self.cfg.exp.log_images:
            self.logger.log_image("train_images", [x for x in inputs[:4]])
        return loss

    def validation_step(self, batch, idx):
        with torch.no_grad():
            inputs, labels = batch
            outputs = self.forward(inputs)
            loss = criterion(outputs, labels)

            top1_accuracy, top3_accuracy, top5_accuracy = accuracy(
                outputs, labels, topk=(1, 3, 5)
            )

        self.log("val_loss", loss.item(), on_epoch=True, on_step=False)
        self.log("val_accuracy.top1", top1_accuracy, on_epoch=True, on_step=False)
        self.log("val_accuracy.top3", top3_accuracy, on_epoch=True, on_step=False)
        self.log("val_accuracy.top5", top5_accuracy, on_epoch=True, on_step=False)
        if idx == 1 and self.cfg.exp.log_images:
            self.logger.log_image("val_images", [x for x in inputs[:4]])
        return loss


@hydra.main(config_path="./cfg", config_name="config")
def main(cfg: omegaconf.DictConfig):
    print(omegaconf.OmegaConf.to_yaml(cfg))

    train_dataset = torchvision.datasets.ImageFolder(
        os.path.join(cfg.dataset.root, cfg.dataset.train),
        transform=build_augment(
            cfg.model.resolution,
            cfg.dataset.normalization.mean,
            cfg.dataset.normalization.std,
        ),
    )
    val_dataset = torchvision.datasets.ImageFolder(
        os.path.join(cfg.dataset.root, cfg.dataset.val),
        transform=build_resize(
            cfg.model.resolution,
            cfg.dataset.normalization.mean,
            cfg.dataset.normalization.std,
        ),
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=cfg.model.batch_size,
        num_workers=cfg.system.num_workers,
        pin_memory=True if torch.cuda.is_available() else False,
        shuffle=True,
        drop_last=True,
    )
    val_loader = torch.utils.data.DataLoader(
        val_dataset,
        batch_size=cfg.model.batch_size,
        num_workers=cfg.system.num_workers,
        pin_memory=True if torch.cuda.is_available() else False,
    )

    assert len(train_dataset.classes) == len(val_dataset.classes)
    model = Model(
        pretrained=True,
        num_classes=len(train_dataset.classes),
        cfg=cfg,
        idx_to_class=dict([(v, k) for k, v in train_dataset.class_to_idx.items()]),
        cutmixup=cfg.exp.cutmixup,
    )

    wandb_logger = pl.loggers.WandbLogger(
        project=cfg.wandb.project,
        entity=cfg.wandb.entity,
        name=cfg.exp.name,
        mode=cfg.wandb.mode,
        # retrieve saved model https://docs.wandb.ai/guides/integrations/lightning#model-checkpointing
        log_model=True,
    )

    trainer = pl.Trainer(
        accelerator="gpu",
        devices=cfg.system.cuda_devices,
        strategy=pl.strategies.ddp.DDPStrategy(find_unused_parameters=False),
        precision=16,
        logger=wandb_logger,
        callbacks=[
            pl.callbacks.LearningRateMonitor(),
            pl.callbacks.ModelCheckpoint(
                monitor="val_accuracy.top1",
                mode="max",
                dirpath=".",
                # concat str with + because config is not in callback's scope where formatting takes place
                filename=cfg.exp.name + "_{epoch}_{val_accuracy.top1:.3f}",
                save_top_k=1,
            ),
        ],
        max_epochs=cfg.exp.train_max_epochs,
        log_every_n_steps=1,
        gradient_clip_val=0.5,
        benchmark=True,
    )
    trainer.fit(model, train_loader, val_loader)


if __name__ == "__main__":
    main()
