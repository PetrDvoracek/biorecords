import click
import os
import glob
import cv2
import numpy as np


def letterbox_image(image, expected_size):
    ih, iw, _ = image.shape
    eh, ew = expected_size
    scale = min(eh / ih, ew / iw)
    nh = int(ih * scale)
    nw = int(iw * scale)

    image = cv2.resize(image, (nw, nh), interpolation=cv2.INTER_CUBIC)
    new_img = np.full((eh, ew, 3), 128, dtype="uint8")
    # fill new image with the resized image and centered it
    new_img[
        (eh - nh) // 2 : (eh - nh) // 2 + nh, (ew - nw) // 2 : (ew - nw) // 2 + nw, :
    ] = image.copy()
    return new_img


@click.command()
@click.argument("DIR")
@click.option("--res", default=512)
def main(dir, res):
    im_paths = glob.glob(f"{dir}/*")
    dirname = dir.split(os.sep)[-1]
    newdirname = f"{dirname}_512px"
    savedir = dir.replace(dirname, newdirname)
    if not os.path.exists(savedir):
        os.mkdir(savedir)
    for p in im_paths:
        im = cv2.imread(p)
        im = letterbox_image(im, (res, res))
        save_path = p.replace(dirname, newdirname)
        cv2.imwrite(save_path, im)


if __name__ == "__main__":
    main()
