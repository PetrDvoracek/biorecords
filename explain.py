import hydra
import torch
from lime import lime_image
import numpy as np
from skimage.segmentation import mark_boundaries
import cv2
import matplotlib.pyplot as plt

import os
import glob

import training

IM_PATHS_WILD = f"/home/pedro/biorecords/fotky_rucne/vazky/*/*"


@hydra.main(config_path="./cfg", config_name="config")
def main(cfg):
    _, model_path = cfg.server.endpoints_models.items()[0]
    model = training.Model.load_from_checkpoint(model_path)
    preproc = training.build_resize(
        resolution=model.cfg.model.resolution,
        mean=model.cfg.dataset.normalization.mean,
        std=model.cfg.dataset.normalization.std,
    )
    explainer = lime_image.LimeImageExplainer()

    def batch_predict(images):
        model.eval()
        batch = torch.stack(tuple(preproc(i) for i in images), dim=0)

        device = "cuda:0"
        model.to(device)
        batch = batch.to(device)

        logits = model(batch)
        probs = torch.nn.functional.softmax(logits, dim=1)
        return probs.detach().cpu().numpy()

    for im_path in glob.glob(IM_PATHS_WILD):
        image = cv2.imread(im_path)  # TODO probably wrong, needs RGB

        *workdir, im_filename = im_path.split(os.sep)
        im_name = im_filename.split(".")[0]
        workdir = os.sep.join(workdir)

        label_str = "_".join(im_path.split(os.sep)[-1].split(".")[0].split("_")[:-1])
        # label_idx = [x[0] for x in model.idx_to_class.items() if x[1].lower() == label_str][
        #     0
        # ]
        pred = batch_predict(np.expand_dims(image, axis=0))
        pred_idx = pred[0].argmax()
        pred_str = model.idx_to_class[pred_idx]
        print(f"predicted {pred_idx} which is {pred_str}")
        print(f"correct label is {label_str}")

        explanation = explainer.explain_instance(
            np.array(image),
            batch_predict,  # classification function
            labels=[pred_idx],
            hide_color=0,
            num_samples=1000,
            batch_size=8,
        )

        temp, mask = explanation.get_image_and_mask(
            pred_idx,
            positive_only=True,
            num_features=1,
            hide_rest=False,
        )
        kernel = np.ones((7, 7), np.uint8)
        mask = cv2.dilate(mask.astype("uint8"), kernel, iterations=5)
        img_boundry2 = mark_boundaries(temp / 255.0, mask)
        plt.imshow(img_boundry2)
        plt.text(5, 5, pred_str, bbox={"facecolor": "white", "pad": 10})
        plt.axis("off")
        plt.savefig(os.path.join(workdir, f"prediction_{im_name}.png"))


if __name__ == "__main__":
    main()
