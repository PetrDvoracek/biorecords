import torch
import timm
import hydra

import timeit
import os


import config

N_RUNS = 10
DEVICE = "cpu"


@hydra.main(config_path="./cfg", config_name="config")
def main(cfg: config.Config):
    os.environ["CUDA_VISIBLE_DEVICES"] = str(cfg.system.cuda_device)
    model = timm.create_model(cfg.model.timm_name).to(DEVICE)
    model.eval()
    batch = torch.randn((1, 3, cfg.model.resolution, cfg.model.resolution)).to(DEVICE)
    t = timeit.timeit(lambda: model(batch), number=N_RUNS)
    print(
        f"inference of {cfg.model.timm_name} with resolution {cfg.model.resolution} runs {t / N_RUNS:0.3f} s on {DEVICE}"
    )


if __name__ == "__main__":
    main()
