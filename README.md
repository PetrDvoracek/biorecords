# Training
Will be added later.

# Inference Server

Info about deploying inference server on Linux system. Inference is on CPU, no NVIDIA GPU is needed. Tested with python version 3.10 (run `python --version` to check your actual version). Should run with any Python version higher than 3.7.

## Installation
Install required python libraries using `pip install -r requirements_cpu.txt`. Depending on the state of Linux system you are using, maybe some additional system (`apt-get`) libraries will be required. 

## Configuration and Run 
Download trained models from [biorecords onedrive](https://365osu.sharepoint.com/:f:/r/sites/Biorecords/Sdilene%20dokumenty/models?csf=1&web=1&e=YL3GMc) or [google drive](https://drive.google.com/file/d/1Zmbzfg1djcgcKLo9dTLo482LfbCTeo96/view?usp=sharing). Change `server.endpoints_models` values in file `cfg/config.yaml` according to downloaded models (`"endpoint_name": "model_path"`). Change `server.host` and `server.port` according to your needs. Run `python server.py`.

## How to get Prediction
Python example on how to trigger endpoint is in `test_server_accuracy.py` file. Curl example follows
```
# trigger inference server
curl --location --request POST '127.0.0.1:5001/predict_dragonfly_small' \
--form 'image=@"/path/to/some/img.png"'
```

## Test Server Accuracy
Download the dataset from ... and change `dataset.root` in `cfg/config.yaml` according to your actual path to the root of the dataset. Run `server.py`, then run  `test_server_accuracy.py`. Test must be performed on `dataset.valid` (validation) part of the dataset, or at least on `dataset.test`, not on `dataset.train`. Test and validation images was never shown to the NN, so it represents reality better than images from train set which was presented to the network during the training.

# Common Problems
```
INTEL MKL ERROR: /opt/intel/mkl/lib/intel64/libmkl_def.so: undefined symbol: mkl_sparse_optimize_bsr_trsm_i8.
Intel MKL FATAL ERROR: Cannot load libmkl_def.so.
```
solved with `export LD_PRELOAD=/opt/intel/mkl/lib/intel64/libmkl_core.so:/opt/intel/mkl/lib/intel64/libmkl_sequential.so`

# Bonus (you do not need to do this)
to install `horovod` on karolina, you need to have g++ version at least 5.0. Install the latest version of g++ with `conda install -c conda-forge gxx`, then horovod with `HOROVOD_WITH_PYTORCH=1 pip install horovod[pytorch]`
