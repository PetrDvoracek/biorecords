import os
import sys
import shutil
import click
import random
import imagesize
import glob


@click.command()
@click.argument("val_dir_path")
@click.argument("output_dir_path")
@click.option("--seed", default=42)
@click.option("--count", default=10)
@click.option("--min-res", default=257)
@click.option("--max-res", default=4_000)
def main(val_dir_path, output_dir_path, seed, count, min_res, max_res):
    if not os.path.isdir(output_dir_path):
        os.mkdir(output_dir_path)

    random.seed(seed)
    randomized_filepaths = sorted(
        glob.glob(f"{val_dir_path}{os.sep}*{os.sep}*"), key=lambda x: random.random()
    )

    counter = 0
    for filepath in randomized_filepaths:
        if counter >= count:
            return
        classname = filepath.split("/")[-2]
        w, h = imagesize.get(filepath)
        if w < min_res or h < min_res:
            continue
        if w > max_res or h > max_res:
            continue
        print(w, h)
        ext = filepath.split(".")[-1]

        # defining new filename
        newname = f"{classname}_{counter}.{ext}"
        # copy the file into the new directory
        shutil.copyfile(filepath, os.path.join(output_dir_path, newname))
        counter += 1


if __name__ == "__main__":
    main()
