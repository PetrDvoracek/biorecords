import hydra
import inquirer

import os
import glob
import requests
import time


@hydra.main(config_path="./cfg", config_name="config")
def main(cfg):
    questions = [
        inquirer.List(
            "model",
            message="Which model do you want to test?",
            choices=list(cfg.server.endpoints_models.keys()),
        )
    ]
    answers = inquirer.prompt(questions)

    model_endpoint = answers["model"]
    url = f"http://{cfg.server.host}:{cfg.server.port}/{model_endpoint}"
    image_paths = glob.glob(f"{cfg.dataset.root}{os.sep}{cfg.dataset.test}/*/*")

    good = 0
    for images_seen, image_path in enumerate(image_paths, start=1):
        path_splitted = image_path.split(os.sep)
        image_name = path_splitted[-1]
        label = path_splitted[-2]

        image_ext = image_path.split(".")[-1]

        files = [("image", (image_name, open(image_path, "rb"), f"image/{image_ext}"))]
        before = time.time()
        response = requests.request("POST", url, headers={}, data={}, files=files)
        request_duration = time.time() - before
        predicted_class = response.json()["predicted_class"]
        if predicted_class == label:
            good += 1
        print(
            f"accuracy: {100*(good/images_seen):0.2f}%, images seen: {images_seen}, duration: {request_duration:0.3f}s"
        )


if __name__ == "__main__":
    main()
